# **OSCE Meets Enspiral** #
#### _A discussion about distributed governance and community, financial sustainability, the [Enspiral](http://enspiral.com) network and [Open Source Circular Economy](https://oscedays.org)._ ####

______________________________________________________________________


**Date:**  Septeber 13th, 2015

**Location:**  [POC21](http://www.poc21.cc/), Château de Millemont, Île de France

**Participants:**

* [Sam Muirhead](https://twitter.com/cameralibre) 
* [Derek Razo](https://twitter.com/DerekRazo)
* [Timothée Gosselin](https://twitter.com/unteem)
* [Mix Irving](https://twitter.com/whimful)
* [Manon Piazza](https://twitter.com/Manon_Piazza)

-----------------------------------------------------------------------


**Sam:**    As a basic understanding of what we’re doing, as I see it, [Open Source Circular Economy Days](http://oscedays.org) is the start of a movement to promote the idea of [open source](http://community.oscedays.org/t/what-is-open-source-video-message/507) as the best - and perhaps only - methodology that we know of which can build a real circular economy. For me, the goal is that when people think of circular economy or want to build a [circular economy](https://en.wikipedia.org/wiki/Circular_economy), the obvious choice or the obvious approach to explore would be the idea of open source.

We believe that at the moment, the companies and organizations that are promoting circular economy are not as effective as they could be because they tend to be focusing on their own in-house solutions. We think that they could be a lot more effective with more cross-industry collaboration, more transparency and more open standards, much more of the open source methodology. We see that open source hardware is quite fertile ground for more circular economy thinking, simply because in order to create open source hardware, it needs to be documented. So there is already an idea of transparency, of opening up the process to other people so it’s an easy next step to make it more circular.

There’s also quite a lot of potential for change here because the companies that are talking about circular economy, one of the main reasons they’re doing it is the bottom line - they’re just saving money, because they're saving resources by reusing them, and not having to dig more stuff out of the ground. They’re benefiting quite a lot by the fact that, A, they can potentially save quite a lot of money, and B, they look good doing it.

There’s a lot of promotion around circular economy, a lot of PR and a lot of lobbying around it as well because it’s something where companies realize that they can get quite a lot of government funding or something if it looks like they’re doing something that benefits the planet. What we hope we can do is to be a reality check on that system and basically say “if you really care about a circular economy, if that’s really what you’re trying to get to, then the way that you’re working in is not the right way. You should be working in an open source way.”

The good thing is that because they’re quite successful at lobbying governments, there is a lot of EU funding earmarked for circular economy and lots of local funding for circular economy, and a lot of research into it. Even though the task of building a circular economy is very difficult (and the task of getting large corporations to even consider working with Open Source is very difficult), we feel that there’s quite a lot of potential, in that there’s a grassroots movement of people who are interested in this, who have little connection between themselves, and no connection to industry and government. If we can get some kind of momentum, then we can start holding the companies and organizations more accountable, and we can also get some of that funding to push open source projects as well.

We’re having a meeting in Berlin next week, with me, Tim, [Lars Zimmermann](http://bloglz.de/), who I work with in Berlin on [Open It Agency](http://openitagency.eu), [Sharon Prendeville](https://twitter.com/sharmarval) and [Erica Purvis](http://technicalnature.org.uk/). They’re both mostly based in London. One thing we want to discuss in our meeting is “what activities do we need to do in order to reach this goal?” 

So far, we’ve organized one event, the [Open Source Circular Economy Days 2015](https://oscedays.org/documentation-2015/), which was a hackathon in 32 different cities around the world, to try and form this network for the first time, and try to get more publicity and start working on practical projects.

We need to work out what kind of an organizational structure and legal structure we need. We also need to work out what other kind of activities we need, other than just organizing events. I think we all pretty much agree that we’d like to do OSCEdays again next year but there should be other things, other events going on, and we should also be building to a situation where we could be doing research projects, where we can be doing policy. Where we could be basically coordinating a lot of the work that nourishes and protects an open source hardware movement for the circular economy.

We provide resources to help people to develop this. We connect people and to allow them to get the most out of their ideas and projects, and we defend the community against threats, whether they be legal attacks or bad policy or otherwise. Have I missed anything?

**Tim:**    	It’s pretty complete, I think.

**Derek:**    	I love what you said about the goal is to be coordinating work that supports the open hardware ecosystem. I’ve been toying with this idea of, I think I’ve told both of you about it, the idea of institutional enterprise. What does a thing that’s not a government and not a company, but is like a steward of commons look like? All these questions are the important questions. What do you actually need to do? What are your products? What is your org structure? How do you actually protect values and an organizational structure and a legal structure? Big questions.

In terms of the big questions that you said you had, earlier today, you gave me a list which was like, what activity should we do? What should our organizational structure be? What events should we run? What else was there?

**Sam:**    	What resources do we need to provide? One example would be a definition, that’s something that we started, but we need to have, like the [Open Source Hardware Association](http://www.oshwa.org/). They steward the open source hardware [definition](http://www.oshwa.org/definition/). There needs to be a community-agreed, strong consensus definition of Open Source Circular Economy.

**Tim:**    	Yeah, and there's governance work and participation work.

**Sam:**    	The idea of defending the community is something similar to, you know, we are able to do all this wonderful open source stuff that we do because we’ve got organizations like the [EFF](http://www.oshwa.org/definition/) who are spotting dangerous things, who are lobbying against bad policy, raising awareness, who are sending in lawyers. When you’re up against the companies who want to do it their own way, that makes things tricky.

**Derek:**    	Interesting. Just to clearly understand what you want to get from this session that we’re doing here, what kind of things do you want to talk about? What kind of outcomes, purpose do you want to this?

**Sam:**    	As I mentioned to you earlier, for me I want it to be so that it’s not 'this is a project by Tim and Sam and Lars and Erica and Sharon'. This should be an organization that anybody can approach from wherever they are in the world, from whatever kind of society or background they are in the world. They can read it, understand it, understand the values, and the goals, and think, “Okay I share those values and the goals,” and there shouldn’t be a barrier in terms of, “Oh no, but that’s some guys in Berlin. That’s their project.” They should feel valued and welcomed.

**Derek:**    	That’s that governance, participation, additional content. How do you build content which is inclusive?

**Tim:**    	Yeah, to keep it decentralized. Really decentralized.

**Sam:**    	Yeah, basically how to slowly make it more and more decentralized. We have a pretty high bus factor already-

**Derek:**    	Bus factor?

**Sam:**    	For example, if Tim and I are hit by a bus, OSCEdays should survive. There is enough distribution of knowledge and responsibility. The 'bus factor' is the number of people who can be hit by a bus and the project still survives. We want to improve that bus factor, you know... if we get a really nasty bus at some stage.

**Derek:**    	It’s the classic downfall of the charismatic leader, right?

**Sam:**    	The bus?

**Derek:**    	The bus. Okay, so… What you want to talk about right now?

**Sam:**    	What kind of a board structure or foundation structure do we need? What options do you think there are available? How could we set it up in a way where we can help steward the movement in these early stages where we’re still just… We’ve got a pretty clear set of values, but we need to nail them down to a certain point, and also get the definitions sorted out and just get momentum to reach the first stage where we’re moving towards financial sustainability, where we’re moving towards having strong local chapters, and then being able to extract ourselves from that situation and it not be reliant on the people who started it.

**Derek:**    	Totally. Yeah. That’s a big project. We sometimes joke about starting the Ex-Founders Academy, which is like for people who started something and are 4 years in and they’re like, “Okay, I want to do another thing, and this thing needs to be sustainable.”

I think starting with that purpose really clear from the outset is a big first hurdle that you’ve already jumped.

**Sam:**    	All of us do plenty of other things. It would be really nice if this thing existed in the world, and it would be really nice if we didn’t have to do it. 

**Tim:**    	I think OSCE should live by itself. When you’re doing your own activity, that makes the OSCEdays. And vice-versa, OSCEdays sustains you, it also brings you benefits and resources.

**Derek:**    	For tonight it seems like it would be a full plate to talk about org structures and the outcome would be some things to research. Some more thinking to do, but at least having some leads.

**Sam:**    	A couple of key words, ideas, and people who might be useful.

**Derek:**    	Yeah, totally. Second thing is talk about processes. You mentioned consensus building. You mentioned fundraising and stuff like that. You mentioned a bunch of things about community, so talking about processes and what works in our experience, what doesn’t work, what are some easy wins, what are some longer goals? For that, the outcome might be some immediate processes to try out, things we can put into place quicker rather than having to build a bunch of new things. Then you talked about a strategy for financial sustainability, just a longer term goal which is around creating a resilient organization.

Do you guys have any org structure right now?

**Sam:**    	Right now, we have no legal form. I think for the international OSCEdays we are 5 people who took on most of the responsibility of setting things up, discussing all these big questions, the people who started it.

**Derek:**    	Founders.

**Tim:**    	I like to talk about ourselves as 'initiators'.

**Sam:**    	Yeah, initiators is good. That’s good.

**Mix:**    	Were there people outside of that as well contributing?

**Sam:**    	Yes. In each local city, there were people who were key contacts, and who would take on organisational aspects.

**Derek:**    	Yeah okay, so you had local leaders basically.

**Sam:**    	Yeah, so even in a city like Berlin where Lars and I, we’re both based, we had a really tight team in Berlin with [Alice Grindhammer](https://about.me/aliceaudreygrindhammer), and a huge big [fashion and textiles](https://oscedays.org/circular-textile-challenges-berlin/) section,  and a couple of people who are really, really active there. We had a team of about 25 people in Berlin who were helping us organize that local event.

**Derek:**    	Nice, wow. There’s like 25 in your community in Berlin?

**Sam:**    	Yeah, and then we had 100 or 150 along for the event.

**Derek:**    	Okay. Org structure is a big one, right? You mentioned a whole bunch of goals around distributed organization and something that doesn’t rely on you in the future. Something whose values can’t be co-opted, is something you said earlier. Have you put any time in to researching on this? What’s the current feeling?

**Sam:**    	[Open Knowledge](https://okfn.org/) for example, I’m not too sure what their legal structure is necessarily but they are an organization that… They host the Open Definition. They host events. Also, when there is project which is like a fledgling project which might need a little bit of infrastructural support, that can become an Open Knowledge project. They have strong local chapters, which are independent but still linked.

**Derek:**    	They’re a foundation?

**Sam:**    	They’re a British foundation. In terms of the legal structure that we have been looking at, we’re looking into being a German [Verein](https://en.wikipedia.org/wiki/Eingetragener_Verein), which literally means club or association. [Wikimedia Deutschland](https://wikimedia.de/wiki/Hauptseite) is a Verein, so they can be quite big, or they could be your local community gardening club.

**Derek:**    	I imagine that you’ve thought about club, company, non-profit, as probably the set of things that are within your scope. Can I just ask what is the goals of your organizational structure?

**Tim:**    	That's the thing. Personally for me, I don't know.

**Sam:**    	I feel like we will have to deal with money at some stage.

I don't know that for sure. In terms of making profits, I don't think that this legal structure that we're looking for has that role. Lars and I for example have [Open It Agency](http://openitagency.eu/) which is our open source consulting agency. It's clearly a for-profit thing. And I'm sure that we would make money based upon our reputation, network, and knowledge of open source circular economy.

But we want a lot of other people who are involved in the network to be able to do that as well.	This OSCE organization has no profit-making requirements or instinct. It's a stewarding organization.

**Derek:**    	Okay. But you want to be able to take money. What institutions are giving funding for this type of thing right now?

**Tim:**    	Big companies give money for that. [Suez](http://www.suez-environnement.com/) gave us money, and [Veolia](http://veolia.de/).

**Sam:**    	Waste management companies.

**Mix:**    	Is it like, "Here is a gift. It's a tax write-off for us," or is it, "This is a grant. We expect a report"?

**Tim:**    	We were branding for them, I guess.

**Sam:**    	In our Berlin event, Veolia was paying for a [workshop](http://community.oscedays.org/t/challenge-imagine-zero-waste/314).

**Mix:**    	That's consulting.

**Sam:**    	Yeah, that was consulting.

**Derek:**    	Yeah, and you did that through your consulting wing?

**Sam:**    	Actually we did that through another Verein, it was a temporary setup. We just needed a place to throw the money to get it done. I don't know if that's something that in the future we would do as OSCE or whatever this is, or as Open It...

**Derek:**    	I can tell you a little bit how there's a pattern with [Enspiral](http://www.enspiral.com/), which is basically to say somebody from Enspiral gets a lead, and that lead passes through something like question filters like, "Cool. Is this a straight-up consulting gig?" If it is, it gets passed to Services because the point of the foundation is not to create a relationship with consulting. Alternatively, we've had several experiences where people want to give us grant money and for those types of situations we use the foundation as the home. 

We actually don't even really use the foundation. The foundation is a limited liability company and we looked into it being a charity, and it just turned out that it was a big fucking hassle.

**Derek:**    	The way that we've got around that is just with having a really cemented understanding of fund-holding partners, so we usually send them a memorandum of understanding which is like, "Here is exactly the terms of our relationship," and what it usually looks is you are going to be the charity that does all the admin related to us getting this grant. It means you're going to take the money, you are going to give the money to us, we're going to send reports to you. You're going to send reports back to the grant holder. It doesn't always work exactly that way but that's it, roughly.

**Mix:**    	It's about the Enspiral Foundation shepherding and making sure that things are being delivered in line with [values](http://www.enspiral.com/about/) and quality.

**Derek:**    	The foundation isn't anyone. It's owned by all the members. There's around 50 of us.

**Sam:**    	This is the non-financial share idea, right?

**Derek:**    	Yeah. You can never sell your share. You can only just give it back so that's like ... That group of people is like the core group of people who have gotten over the trust barrier. The trust barrier is like we have worked with you for over a year basically.

**Mix:**    	And you seem like a really good fit and we would trust you equally to make decisions.

**Derek:**    	Yeah, because what ends up happening is the distributed accountability module is installed at that layer. For example, somebody who is a friend of the network put Enspiral as one of the communities using their technology - and it's kind of true and kind of not true.

What Alanna brought up when I pointed it out, she was like “an Enspiral member probably told them it was fine” and that's the whole point of that being there - so that somebody who is a steward can steward...

**Sam:**    	We had that with [Edgeryders](http://edgeryders.eu/) a little bit as well.

**Derek:**    	Really?

**Sam:**    	If I saw an opportunity for funding or a project or something that required an institution, and I am just me, or Open It Agency, then I could go to them and say, "Hi, I'm Edgeryders. We have worked with UNDP, UNESCO, Rockefeller. We have done all of this stuff with Council of Europe. We have responsibility, structure and stability. Let’s talk."

**Derek:**    	Nice. Yeah it's really useful, as long as the group of people who have that power are really trustworthy.

**Mix:**    	It's interesting. We were just talking with Manu from [Materia Brasil](http://materiabrasil.com.br/), talking about they managed this and their collective, [Goma](http://goma.org.br/). 

You are talking about leveraging the brand to get work, and she was saying that that was a very sensitive topic for them - maintaining the integrity of the brand is also really important because it's such a powerful thing. They came up with some interesting solutions which is just like a very high bar. You have to have at least 3 people sign their name to it and everybody has to agree on how... You've got a good financial plan and you're not going to fuck it up for everyone, because that's like 75 people's livelihood attached to it so it's a little bit different.

It's bus protection. Well, not buses, just people fucking up.

**Sam:**    	Even with Edgeryders you still have to go back to the core group and say, "This is what I have planned. Can we do it? How can we do it?" and it has to go through those proper systems.

**Derek:**    	It sounds like the way that your thing has emerged is really similar to a way that a bunch of these things seem to be emerging which is like there is a common asset which in this case is like this immaterial commons - the event materials and the assets that you've got and the brand of the open source circular economy and stuff. Then you've got the consulting wing which is like basically a livelihood tool. It's like “this is how we make our money”. And interesting to notice there... I've seen a bunch of communities that run into a lot of trouble when people start relying on the structure that should be the steward, the foundation, for livelihood.

**Sam:**    	Right.

**Derek:**    	Having really clear delineations about what structures are for what, like the foundation may not be for people's livelihood. In the future it may employ lawyers, right? It may do whatever, but decisions about that are not based on people's need, really. It's based on the need of the mission that that thing is stewarding.

**Mix:**    	It's a way to protect that mission from being corrupted by me just really needing to pay rent.

**Derek:**    	The cool thing is the consulting wing can actually really effectively hold that need. It's like if you've got more consulting work than you could do, you can help other people make a livelihood, but you're only ... You're going to do it not necessarily through the organization that's the foundation.

**Mix:**    	The other thing I heard was that you are proposing solutions for organizational structure before knowing all the needs. There's always going to be this tension, but jumping to the solution ...

**Sam:**    	The issue was that we decided in November to do this event and really committed to it and spent the next 6 months just working hard at it. In that time we were really desperately trying to get funding for the event. There were a lot of opportunities that we weren't able to take advantage of because people needed to have... Well, they needed us to have been funded by someone else before. The usual story.

But secondly, we needed to have a proper legal structure in order to do that.  That was why we felt like that should be something that we should do, but also... with the trademark, like the [OSCEdays branding](https://cloud.oscedays.org/index.php/s/sRzd31xOceo8ejN?path=%2FOSCEdays%20Logos%20%26%20Branding) for example. At the moment we've just got that under a CC-BY license with a note saying “well, this is a trademark. You can use it, but if the members of the OSCEdays community decide that your use is not in line with our values (with a link to the values), then we reserve the right to deny you the use of the branding.”

Even though it's not registered as a trademark, even though the OSCEdays core team or whatever isn't a real thing, even though copyright licenses don't apply to trademarks… but you know.

I think we feel like some kind of structure would help to clarify-

**Mix:**    	and support that, yeah.

**Derek:**    	One thing that you might do is clarify that legal structure in a non-complicated way and then get a really cemented relationship with a fund-holding partner. I think that's one way to go through. The trade-off there is that you end up usually paying them if you make ... If you get a $100,000 grant they will take 10 grand or something, which is not ideal but to actually set up an institution that can directly take that type of money, especially government money, is usually difficult. It might be maybe-

**Sam:**    	We might have that in the [Renewable Freedom Foundation](https://renewablefreedom.org/), which is an organization in Germany. It's a small foundation that... they support TOR and open source decentralization projects. That could be an option. They are small but they have connections to bigger organizations and they understand the system. 

**Derek:**    	In general, I think the key... well, I don't know anything about German legal structures.

**Mix:**    	Disclaimer.

**Derek:**    	Disclaimer. The advice I have always been given by people who are a lot smarter than me about this stuff is clarify your goals for your organizational structure, and create the simplest possible organizational structure that allows you to meet those goals without necessarily-

**Sam:**    	Without getting too complicated at this early stage.

**Derek:**    	Yeah because you could ... You can literally spend years of your life doing bureaucracy to set up an organization that then creates more admin work for you.

If you don't already have a strong set of admins who really like writing, filling out forms... What I was hearing from you is like you want to be able to take money. You can do that through fund holders. You want this thing to steward some of the commons so maybe if this organization is the one that owns the IP...

**Sam:**    	In terms of having a [Contributor License Agreement](https://en.wikipedia.org/wiki/Contributor_License_Agreement)?

**Derek:**    	Yeah.

Then it seems like you have this big question which is around good membership. What organizational structures can give you a good protocol for membership? There are some that are going to be like ... From my experience in the States, it's like your organizational structure ... There is the [LLC](https://en.wikipedia.org/wiki/Limited_liability_company), which is like 'do whatever the fuck you want'. Great. You can just write a co-op into it if you want to do. It doesn't matter, but there are other organizational structures like non-profits. I was a part of a co-op that was a non-profit and about halfway through my time there, they were approached by a lawyer who was like, "It looks like you are doing direct governance. You are legally not allowed to do direct governance."

**Sam:**    	American non-profits...  yeah, I know the Open Source Hardware Association has had difficulties. They are not allowed to be directly supporting businesses, and when you're the Open Source Hardware Association then that's what you should be doing - you're trying to grow open source hardware businesses.

**Mix:**    	It's fucked. It's so fucked.

**Derek:**    	It's based on market dichotomy, right? It's like there is private sector and there is shitty non-commercial, non-value creating, non-profit sector. What you're actually going for is the thing which is supporting an ecosystem of private sector things and reaping a little bit of reward which is reasonable.

**Sam:**    	I know that the German Verein works for both Open Knowledge Foundation Deutschland and Wikimedia Deutschland – they are Vereins. They at least provide services for money and take government funding and organize events and run projects. I know that it's more flexible than a [501(c)3](https://en.wikipedia.org/wiki/501%28c%29_organization).

Although  both of those organizations are more traditional than what we hope to be. I mean, we are not quite ready to [put it all in the blockchain](http://www.shareable.net/blog/interviewed-joel-dietz-on-distributed-collaborative-organizations) but I think we are also not as old school as a lot of the more bureaucratic parts of Wikimedia.

**Derek:**    	Sure. It sounds like an action point of actually just talking to some of those people and being like, "How does this work for you? What are the terrible parts about it? What are the really good parts about it?" and getting a sense for that. I think this thing about getting ... The big things here are money and membership. If you have a good group of people who are on your board or whatever the organizational core is, then stewarding the commons is going to be about your relationships more than it is about the organizational structure, but as long as you have good memberships and a good way of taking money, that's pretty sweet.

**Tim:**    	Is the money about ... I'm not sure about what do we make with this? What do we do with this money, like if we get 100,000 euros right now?

**Sam:**    	Launch a research project, for example.

**Tim:**    	It's not linked to the event.

**Sam:**    	Not necessarily linked to the event, no.

**Tim:**    	For me this part is important, I think, in that no cash flows should go in any centralized or global structure for the events.

**Derek:**    	The event is only probably one of the activities that the ... I don't know if it's a foundation or network or whatever but the organization that you're talking about stewarding, the event is one part of it, right? The other part is all the assets that the event generates.

**Sam:**    	Yeah. The event should not be too expensive to put on in the future, because we've done a lot of the work setting it up already. It's distributed anyway so people just pay for their own events however they can.

For the events we will still just have our local financial systems. That's what we would do, so in Berlin, Lars and I would probably work something out with Open It Agency and Circular Lab, for example.

**Derek:**    	That's an interesting question that brings up, which is like is this thing that you're talking about creating, the coordination body for the event, or is it an organization in which the events live in?

**Tim:**    	For me I would just say this organization just capitalizes on all the assets that are generated from the event and from the community that comes with the events.

**Mix:**    	What do you mean by capitalize?

**Tim:**    	Takes data and make something with it, something useful.

**Sam:**    	Like resources.

**Tim:**    	And policy making stuff.

**Mix:**    	Not making money.

**Sam:**    	No, not making money.

**Derek:**    	There is a thought that I started that I didn't end up finishing which was about the relationship between Enspiral Foundation and Services. With a bunch of these types of organizations, it seems to be that what often will happen is a big grant will come into Enspiral Foundation. We will have a community decision on how to spend it. People will make proposals. It will iterate towards some sort of consensus.

Usually what actually happens is nobody puts in the work, and a couple of people who are really passionate about something put up a pretty good proposal and it goes forward because the opportunity is there, which is fine. Then what happens is Enspiral Foundation hires Enspiral Services to implement it because most of the people who are floating on the Enspiral Network if they have done any work, they have probably done it through Services. What happens is Services becomes the contracting company of choice of the thing that doesn't necessarily make money, and so you get this dynamic which is really nice where there is a separation in the API - like this thing can still make decisions about how to spend those resources and then you utilize the network of people who need a livelihood to make those outcomes happen.

**Sam:**    	For me, that pushes dangerously into the Edgeryders territory where you only have one company that can get financial value from the network and the assets.

**Derek:**    	Right.

**Mix:**    	There's lots of groups within Services.

**Sam:**    	Maybe you can tell us how Services is structured and that will help us understand a little more.

**Mix:**    	Quite loosely, there are teams within Services. The teams don't have a legal boundary so much as like an agreement boundary. I am part of [Craftworks](http://craftworks.enspiral.com/) which is a bunch of developers and sometimes some designers. We do a particular type of application online. Then there are other groups within Services who do just simple websites.

**Derek:**    	The thing about Enspiral Services is that it's a tool of the network and the network is made up of Services and the Foundation. Neither is independent of the other necessarily. They are, but they're not. They're interdependent but yeah, it's just like Mix says, there's teams and we have a clear policy for how you propose a new team. For example it's basically like, "Here is how you abide with our legal structure." The benefit of this consulting company is that all of these companies get things like a shared bank account, shared accounting services. There's a legal company. For example [CoBudget](http://cobudget.co), we're a year in. We have no legal structure. All of our money has come through Enspiral Foundation and is paid to us through Enspiral Services as contractors.

We have a clear team within Services because we've created the relationship there. We have adapted ourselves to the policy, but it's given us this bubble that we live in. In Enspiral Services there are also just individuals, like freelancers who are friends and they are like, "Oh man, I have to do all this accounting bullshit," and then, "Oh well, Enspiral Services employs an admin." The admin is one of the shared resources of Services, so Services is more like a co-op.

**Sam:**    	Okay. In our situation for example, Open It Agency which is me, Lars, [Shure](https://twitter.com/AlexShure) and [Jenni](https://twitter.com/JOttilieK), we would be like a little dot in this thing or we would be like a whole section of consulting?

**Derek:**    	You'd be a team.

**Tim:**    	And can I say, "Okay I don't want to be near this Enspiral bubble anymore. I don't like it anymore," and I just go?

**Derek:**    	Yeah totally, but you wouldn't get the services anymore.

**Mix:**    	Then you would have that challenge of whether the foundation is only able to fund teams inside that bubble.

**Derek:**    	I don't think so. Well, can you explain?

**Mix:**    	Describe an instance where the Enspiral Foundation has funded someone not within the Enspiral Network.

**Derek:**    	The Enspiral Foundation's _point_ is to fund the people inside the Enspiral Network. 

**Mix:**    	But I think Sam was saying-

**Sam:**    	That's also the Edgeryders' idea as well, to fund people within the Edgeryders network and... 

**Mix:**    	You said that's good or bad?

**Sam:**    	No I didn't say that's good or bad. I'm just saying they have the same stated goals and my experience with it is ... Well, they funded me, and I was within the network and it was great, but many people feel that they have allowed it to become too focused on consultancy and on doing whatever jobs the UNDP wants them to do, which means it dilutes the focus from the things which many people came to Edgeryders to do, which is to build projects like [unMonastery](http://unmonastery.org/) and [Makerfox](https://www.makerfox.com/) and...

**Derek:**    	There is a big piece of this which we are not talking about at all which is what culture do you build, right? Those types of problems sometimes, it sounds like, can be the root of really difficult cultures where people don't feel ... Part of the design of the Enspiral Network is, when I say design I mean undesign, some of the values behind it there were like you should be able to leave, nobody is required to give money to the [CoBudget](http://cobudget.co/) process. Nobody is required to be an Enspiral member. The idea is that the network generates so much mutual value that it's just worth being a part of it and being a good person to the other good people.

I see what you mean, though. The idea of this legal structure getting too tied to a service company doesn't seem like an immediate danger.

**Sam:**    	I'm not thinking that it becomes too tied to Open It Agency, but that there could be some development whereby the activities of OSCE Foundation or Network or whatever it is, the activities of that group become too focused on the consulting side of things and less on supporting the community and defending the community and all the other things that we mentioned before. As you were saying, you want to make sure that you don't get too distracted by “I need to pay my rent”.

I don't know if ... I'm still trying to work out in my head if that structure that you guys have with the Foundation and Services, if that effectively deals with that problem or not. I'm not entirely sure if it does or not.

**Derek:**    	Definitely heaps of people just end up doing lots of consulting, but it's not necessarily because those people don't want to be a part of the social value the thing is creating.

For example, [Open Source Open Society](http://opensourceopensociety.com/) was a big conference that we did, but it was really nice. We invited a bunch of people from government and a bunch of people from business and from the open source community and... I heard somebody saying while I was walking around, "Wow, this is the most well-resourced conference I have ever been at." Totally not true, but the reason was because you've got heaps of Enspiral people who are just fucking into the idea of Open Source Open Society and they were just volunteering, so it was heaps of volunteers. The only reason that they can do that is because they make a living and so there is this… but I totally see what you mean.

**Mix:**    	I've got an unfavorable position in that I end up doing a bunch of consulting so that's 60 to 70% of my week is consulting. Not consulting, but contract work. It's stink because it's not as exciting but I get to work in an environment that's really supportive and I get to have really interesting conversations and I get to do whatever I want whenever I want, pretty much, and I can contribute the other 30% of my time to these projects. When that conference is on I can just drop my stuff and go make it better and connect people, just turbocharge things that are already going really well so I don't feel like ... I take some of my funds and put them in the middle which means that people like Derek can work fulltime on doing rad shit and can do more rad shit.

It's not ideal, but I don't feel like I'm distracted from the mission per se because I am helping it grow faster and further, and I'm  living the values that I want to live, which is immeasurably valuable to me.

**Sam:**    	I think that's something that Edgeryders for example doesn't have - that everyone puts in 5% or 6% or whatever it is.

**Derek:**    	There is another layer to that which is that Services gives money back to the Foundation, so Services ... In the same way that a team has a relationship with Services which is about giving a contribution, all of Services has a relationship with the Foundation which transfers the foundation money. That's the only way that anybody gets to do mission work, really. It's like the Foundation has an ongoing stream of funding because the consulting is successful.

**Manon:**    	Does this consulting thing work just because you are a lot of people in Enspiral?

**Mix:**    	It started pretty small, like 5 people, but it  definitely works better when there are more people.

**Derek:**    	Yeah. I guess part of it is that it gives you a structure to put people into. They're like, "Oh, I want to help out," and you're like we don't have any money and we are managing this big thing, but then a consulting gig comes through and you say, "We can work on this together," and that would be one way for us to work together, get to know each other. We could talk about the mission and actually resource ourselves to do the mission when we can.

**Mix:**    	Interestingly, upscale heaps on massive amounts of personal growth with professional growth, to the point where it's easier to do more and more high-paying consultancy if you want to, which means you've got more and more time for ...

**Sam:**    	For the other interesting stuff there.

The other thing that would be useful is if you know of ... You have other examples of structures and organizations that you think are interesting that maybe don't work necessarily for Enspiral that might be relevant for us to look into as well, because I have only looked into the ones that I happen to know personally.
I have a basic idea of how the [Document Foundation](https://www.documentfoundation.org/) or [Linux Foundation](http://www.linuxfoundation.org/) work or something like that, but if there are others that we might not know or that you find particularly interesting, that would be good to know about so we can research them before we have our meeting.

**Derek:**    	The impression I get is nobody gets away from the livelihood question. When we were in San Francisco, me and Alanna, we had a meeting with Sue Gardner who was the president of Wikimedia through its big growth phase until a year and a half ago. Her feedback or one of the things she said was just that it was really difficult to be constantly out champing at the bit for funding. It's a nightmare situation where you end up fighting a bunch of weird battles that you don't actually want to be fighting. Funding for organizations like this thing that you want to build is shitty.

I have this intuition about financial sustainability as being the core of these types of new human institutions. Something really changes about the way that you think about your work when you can make a living doing the work that you love.

**Sam:**    	It's just financial sustainability, basically.

**Derek:**    	What's the experience that you have right now? Where is your income coming from?

**Sam:**    	My personal income comes mostly from [Camera Libre](http://cameralibre.cc/) - my videos - and that's all from open organizations, and then also somewhat from Open It Agency, which has been just workshops and one-off consulting things with either small-scale makers, helping grow communities or develop business models, or else with established companies who want to try this open source thing.

**Derek:**    	Yeah. Is your ideal world one in which you're working on this, the work at the center, the core work of the open source circular economy foundation? Do you want to be doing that full time?

**Sam:**    	For me, no. I want that to be a maximum of 33%.

**Derek:**    	Oh okay, cool so ideal is ... What do you want to spend the other 66% doing?

**Sam:**    	Camera Libre and Open It Agency at this stage.

**Derek:**    	It sounds like you're all freelancers already, right?

**Tim:**    	Yeah that was the idea actually. None of us really wants to work only on this. For me I wouldn't mind one year just kick-starting it but going away quickly. Each one of us wants to be like a freelancer, to have our own projects, which will be directly linked to the OSCEdays.

**Derek:**    	Yeah. It sounds like a lot of freelance work that you're doing is directly related to the mission. Do you want to talk about business development of a freelancing agency or do you want to talk about  the business development of the foundation? In terms of financials, what input are you looking for? 

**Sam:**    	I know what we need money for, which is things like lawyers and research and stuff like that.

**Tim:**    	What would you say is the research part?

**Sam:**    	One of the main things we need to work on to convince people is the evidence that open source works in industrial processes and things like that. Yes, we need to be generating projects but we also need to be doing more research into open source circular economy projects within the network, to actually measure whether they have the effects that we say or hope that they do, and whether they do increase 'innovation' or whatever.

**Derek:**    	Yeah, right. I'm sure there's heaps that are already doing that research who, depending on how this consulting goes and the mission of the foundation goes, they may approach you.

**Sam:**    	Erica and Sharon have actually joined an [RCA research project](http://futuremakespaces.rca.ac.uk/) looking at circular processes in FabLabs and makerspaces.

**Tim:**    	For me the things that I would want to foster are... there is a big group of circular textile designer experts in Berlin. We get funds for them and we can make the research and plug in with some things like OSCEdays.

**Sam:**    	Or we can make a collaboration platform for their projects, because that's what's missing.

**Tim:**    	Actually that's something I would like to see - we provide the kind of infrastructure, web tech infrastructure and research for those groups.

**Derek:**    	Just to clarify, one question that I would ask is how much money do you need? What do you want? How much do you want in your ideal world?

**Sam:**    	That depends, because there are certain things like the policy stuff. I can't imagine anyone wanting to do sustained work in policy, on behalf of open source circular economy, for free.

Also, there are a lot of different points to influence EU regulations for example, but getting involved in that nasty quandary would have lots of costs as well.

Then there's also things like ... Money can go to things like development of technical solutions, so things like developing [OpenLCA](http://www.openlca.org/) for the circular economy. OpenLCA is a life cycle analysis tool, it's free software. It's ridiculously complicated (as all life cycle analysis tools are) and at this stage it's just used for analyzing cradle-to-grave products, so basically how much energy is used, how many resources are used in the manufacture, distribution, use and disposal of a product.

We would like to basically adapt this to analyse things in terms of circular economy and to try to link up those inputs and outputs to other manufacturing processes, but that would require a whole lot of software developing time. There's a couple of other core software projects which we would want to develop too. That's the thing which if one of the five of us, for example, was a talented developer then we would just start it as an open source project, get the minimum viable product going and get people on board contributing and run it. We know how to build a community, but none of us have the skills to be able to keep the technical side ticking over and so we need to find or hire someone...

**Mix:**    	Yeah of course. It sounds to me like a bunch of different threads. You've got a bit of a Theory of Change about how to change the system.

**Sam:**    	Yeah, the answer to 'how much do we need' is 'as much as you can give us' because-

**Tim:**    	We'll find something.

**Sam:**    	There's a lot of things that we could do with funding.

**Mix:**    	There are different things like paying people within the network and then this attracts people in to work for the network.

**Derek:**    	One thing you said to me which really stuck with me was open source circular economy is not a 10-year goal. It's a 300-year goal.

**Sam:**    	The working circular economy part, yeah. What is the 10-year goal at least is getting the idea of open source inextricably linked to circular economy.

If we succeed at that, it makes the 300-year goal maybe into a 100-year goal.

**Derek:**    	Totally, yeah. That's a really nice way to put it strategically. Ten years ... It depends on the way that you want to go, and it depends on your skill set. You could spend all your time writing grants. You could do that. That's a totally viable way to do it, and that would ... That's one way to become sustainable in the sense that eventually you make enough grant money to pay people to write grants at a pace, which gets you more grants so that you can keep paying them to write more grants.

But from minimal experience with organizations like that, I think it's like... hell. 

**Mix:**    	It introduces an interesting dynamic as well. There are big chunks of money and you've got to jump between them.

**Derek:**    	Yeah, yeah. I think it's worse than being focused on a consulting company, just to be perfectly straight up. And in terms of the way that the power dynamic is set up, at least with clients there is a value exchange and you can build a reputation of doing good work that goes beyond that one client, but in these foundations the power dynamic is so one-directional. It's really icky.

**Mix:**    	There's one strategy with Enspiral which is 'get really good at what you do and start charging more money, and get your friends doing similar work and bring them in too. You find people who are values aligned, upscale them in whatever skills they have got like support them to get better, and then it will just happen.

**Derek:**    	Do you just want to talk about different options or scenarios? I think consulting is probably the ...

**Sam:**    	The best bang for your buck.

**Derek:**    	I think it's tricky because, like you said, you will spend a lot of time consulting, but the better you get at it, the more you do it, the more you can twist it in the direction that you want it to go. If you become the $250-an-hour consultant who comes and tells the company why they are shitheads for not being open source ... There is this interesting positive feedback loop which is like you become a strong commercial player which actually makes you even stronger on the non-commercial side because now you've got the experts. For example our Loomio consulting is like $2,000 for 4 hours and so we have-

**Mix:**    	Which sounds expensive, but it's a lot of work.

**Sam:**    	Yeah. It's not really just 4 hours.

I think we are in a situation where that can become quite viable quite quickly, companies find our approach interesting at least, even if they aren't convinced yet. Our [Eindhoven](https://oscedays.org/eindhoven/) event was entirely run by Philips at the Philips headquarters, and they loved it basically. Philips is probably the biggest company that's committed to circular economy (as they understand it) and so I think we've got potential there.

**Tim:**    	How does it look like if we do consultancy?

**Mix:**    	I reckon that would be a really valuable conversation because at least we have a lot of experience and then we can perhaps swap ideas on what's your approach to consultancy, like how do you picture it? What are your boundaries? Blah-blah-blah.

**Tim:**    	For me, the part that scares me with the consultancy direction, is what it can do to communities. We know how to facilitate workshops. We know how to engage people. We know how to create communities. So, OK, we have to make a livelihood of it. We're going to do consultancy. 

**Mix:**    	That's really valuable.

**Tim:**    	It is, but now it's like... I've had experiences doing this, and I found myself… selling my expertise on engaging people, and those people became numbers. I was selling numbers to companies and that was people - like how many people engaged this month? Well, with this event there was 200 people...

**Sam:**    	How many email addresses can you get?

**Tim:**    	That's like, for me, the worst thing that could happen - like you're selling your community, but this community doesn't benefit from it.

**Sam:**    	I think that would be something I'd be very wary about as well, if it's extractive work from the community. If it's just using the resources that the community creates together then I think that's fine, but if it's leveraging those people themselves coming... No, hang on. Maybe I'm not so dead-set on that because that's what we did with the OSCEdays in Berlin and the Veolia workshop. And we had other people there. There actually I was comfortable with it because we made it very clear with Veolia that anything produced in that workshop would be open source, so others can benefit from it.

If that happens, I think I'm fine with it basically...

**Derek:**    	Yeah, yeah. I think this is a really tricky problem and one way that we tend to get around it is… so in that example, a bunch of people from the network show up and their presence is leveraged to create some capital. It sounds like what happened in your case, Tim,  is like it's just going to people organizing those things. They're leveraging the community over and over again.

**Sam:**    	Nothing's going to the participants.

**Derek:**    	One hack I think that we have to get around this is collaborative funding. It's like, "OK, you're leveraging the community. We are going to expect about 10% of the profit from that event to go back into the community."

Keep doing it for sure because the more you do this and the more people are willing to show up ... Nobody is being forced to show up. The more people are willing to show up-

**Sam:**    	The more the community benefits.

**Derek:**    	It goes right to the foundation, right? The foundation is the thing doing the mission work, so you get this positive dynamic which is people show up because they're interested anyways.

This person makes money and can make a livelihood and the whole community ends up getting at least ... There's an exchange there that's like a loop. It's not the perfect thing but it is one way to just close the loop a bit. 

In terms of consulting companies, this is probably our main domain. It's like starting and running consulting companies. We're not experts at it but it has some pretty reusable pieces. Should we just talk about that?

**Sam:**    	Yeah.

**Derek:**    	For context, I ran the sales part of [Loomio](https://www.loomio.org/) for 6 months.

**Sam:**    	You did, or Rick Dazo did?

**Derek:**    	Rick Dazo did. He did a great job. 

**Sam:**    	That's the sales alter-ego - the Sales Douche.

**Derek:**    	Most profitable quarter so far! [Laughs] I got a really good view of what this whole thing looks like. The interesting thing about Loomio as an example here as opposed to Enspiral Services is that Loomio really does not want to be a consultancy.

Loomio only does consultancy because of livelihood. So we've been constantly pushed in the direction of creating distributed consultancy. Similar to what you have with the OSCEdays, there's a set of documentation that you hand out to each group and it's like, "Cool. Here is how you start an event. Here are the things that you need to do."

**Sam:**    	So instead it's like, "Here is how you do consultancy." I like that.

**Derek:**    	Here are the materials. Here is the criteria. Join us. Here is the things that you need to agree to, to join.

**Tim:**    	That's what Romain is trying to do with [Social Media Squad](http://socialmediasquad.cc/). To open the process.

**Derek:**    	It's a hard problem but heaps of people have done it, and people who work in open source have a unique perspective which makes this approach much more intuitive than if you're from a traditional consulting company.

**Sam:**    	Yeah I think basically whenever you say “distributed” both Tim and I are like, ah, “yes”.

**Sam:**    	The issue that we have at the moment is that, although open source is the best way to reach a circular economy, in the short term it's not necessarily the best way to earn money while promoting yourself as circular economy. 

**Derek:**    	[Laughs] Fuck. Yeah, yeah.

**Sam:**    	There are much better way to earn money while promoting yourself as circular economy. 

So our pitch might be uncomfortable for companies. which makes the consulting aspect difficult. The main things that we feel we need to push on are, "Where does your company struggle with something that other companies also struggle with?" and then "Where can we help you work with other companies and people who you can collaborate with to develop solutions?" In traditional business there are many people you can't work with easily. Competitors, or grassroots / activist communities, you know. So what's the situation where you could benefit from their input on developing some kind of project...

**Derek:**    	Yeah, creating a platform. Everybody can join into the conversation.

**Sam:**    	Because I don't think that we can walk in and say, "OK, we're going to double your revenue."

**Derek:**    	Yeah, of course.

**Sam:**    	Because we're going to be like, "Well, we can't promise you anything on revenue, but we'll do lots of other cool things and we'll actually build a real circular economy."

**Derek:**    	Yeah, yeah, yeah. What piece of this do you want to focus on? Do you want to just talk about the whole stack of what it is to run a consulting agency? Is it that specific or do you want to talk about a little more of the high level?

**Sam:**    	One thing is that I can explain things to people very well theoretically, and why collaborating with other companies on a shared project makes a whole lot of financial, social, ecological sense. But I don't know how to be like, "Hey this is the CEO of this company; this is the CEO of that company. And now, kiss."

**Derek:**    	Yes, that's a hard one.

**Sam:**    	I don't know how to do those things. One other problem is that we don't have a huge number of great, glorious successes behind us, in terms of like, "We helped this company do an open source project and they doubled their revenue or got all of this extra stuff." We've helped people start to build communities and things like that, but not on a large scale. We've given people very positive experiences with open source and helped them not run away from it and commit to continuing in developing it, but it's a slow, trickling kind of thing. It's difficult, that's where you get this very strong impostor syndrome kind of feeling. 

Because you're like, "This is all pretty theoretical. It was based on extrapolating real facts from software into ..."

**Derek:**    	Hardware production.

**Sam:**    	yeah… "hopefully, this also works in hardware."

**Derek:**    	Yeah, interesting. I'm hearing that you need some sort of network partners who can make that CEO kiss happen or you need some capacity to do that networking, sales, whatever you want to call that. Then you talked about, probably the prerequisite to doing that is needing this ... This is a bit of a terse way of describing it. ... like credibility. It needs to be an easy yes for someone to be like, "We have a vague interest in this and we're interested in hiring somebody to give us more impression," and it needs to be like, "These are obviously the dudes."

**Sam:**    	Yeah.

**Derek:**    	Okay. What about you, Tim? What do we want to focus on in terms of a consulting thing, like what ...

**Tim:**    	What I want to do, personally?I don't know. I just know that I want to create this thing, the OSCEdays.

**Derek:**    	You said that you do facilitation, right?

**Tim:**    	Yeah, workshop facilitation, connecting people.

**Sam:**    	For OSCEdays for example, he took 25 people to a town called Mouans-Sartoux and worked with the local organizations and government and people to create open, circular projects at the city level. 

**Derek:**    	Right.

**Derek:**    	Do you know people who are making a living doing this type of consulting? Or just this general area of things that you're interested in.

**Sam:**    	In terms of open source hardware, there's one or two people who do it a little bit here and there on the side, people like [Ben](https://twitter.com/btincq) for example, and [Simone Cicero](https://twitter.com/meedabyte) and ... I can count them all on one hand.

**Tim:**    	It's interesting because when I see new leads like it's like... we need the OSCEdays. 

**Sam:**    	When you see what?

**Derek:**    	You need OSCEdays for sales, network partners and credibility.

**Sam:**    	But we need the OSCEdays in like, five year's time, when we've got projects that are tangible examples and have living communities and are making impact. And we have strong connections to companies. 

**Derek:**    	Totally.	Yeah, it's a process.

**Sam:**    	It's about how to get from this current level of no real credibility in the eyes of companies, no deep, strong network ...

**Tim:**    	For me that's not really a problem in this field. 

**Sam:**    	For dealing with Coca-Cola or something like that?

**Tim:**    	I mean they're really looking for what we're doing. They don't know shit about it. The first thing they hear about like POC21 for example, everyone is jumping on it.

**Sam:**    	Well, I think if it's framed in a kind of experimental exploratory way... at least with the Veolia workshop, all they needed to do was throw a couple of thousand Euros at us. It wasn't expensive for them at all. They had a risk-free introduction to a topic. I think that's kind of more where we feel very comfortable, and we feel that we're not selling them something that we can't deliver.

And where we're saying, "This is an educational experience for you guys, really, to understand a different way of doing things." Maybe that's more the way of framing it.

**Derek:**    	It sounds like there's going to be a ramp up, basically, which is like partnerships and leveraging really hard the stuff that you've already done and some sales. Finding out who's interested and really pitching. Being like, "We're the people that do this," and like, "Look at what we've done. We were at POC21. We did Open Source Circular Economy Day. Our consultants include ..." I think you can, not fake credibility, but credibility ...

**Sam:**    	Puff ourselves up a little bit.

**Derek:**    	Credibility is open source. You can fork other people's credibility. Like ask Ben if he wants to collaborate on a project. Fork some credibility.

**Sam:**    	That was one of the first things we did when we were starting this, and also [POC21](http://www.poc21.cc/) was starting. We were like, "Should we be official supporters of each other? Yeah, all right. POC21 is on our website. OSCEdays is on your website." Done.

**Derek:**    	Exactly. So given that, you've got a really strong foot to step forward on. Not only can you be like, "We're the guys that do this," but you can say, "We're the guys that do this in an innovative way." In some cases, if you get into the right situation, you can pitch that to be even more expensive. There's a weird thing about traditional companies where the more expensive you are, the more they'll let you fuck with them.

The best contracts that I've sold with Loomio have been the ones where they're paying a lot, so they want to get a lot of value.

It relies on a strong consultant being like, "No, don't delete that person's comment. Engage with them. Talk to them about why you think that what they're saying is a bad idea," and they get through it, and they have a good experience. They're like, "Ah, okay, I guess it was worth it." It sounds like a big stepping stone is partnerships, getting really clear sets of understandings with people who you're going to see as partners and collaborators. You've already got quite a lot of assets and stuff. What piece of this do you want to talk on? Strategy or logistics or ...

**Sam:**    	Kind of how you get it started, I think.

Because I think a strategy is hard to work out until it's up and running, until we've got some experience in it. Those first steps like, "How do we go from now until six months? How do we get this started from now until next year, basically?"

**Derek:**    	The tricky way to say it is like, "Start making money." The less tricky way to say it is like, "Get a really clear definition of what you're offering," like partner with people. Deliver some shit. Make it public. Write about it.

I think if you just look at this as a loop that you can do for as long as you want until you've got the credibility to do some of the bigger projects that you want ...

Yeah. Generating content is going to add to the credibility that you want to get and the ability to get money and partners and networks and stuff like that. In terms of getting it started, that would be the strategic angle of getting it started, literally just talking to Ben and being like, "Ben, let's do a consulting gig together." Just start with one thing. 

Just be like, "We've already done these things. Here's our skillset. We obviously can do consulting on open source. Let's exchange slide decks. Let's sit down and like, 'Show me what your offering is. We'll show you what our offering is.' Let's find something that actually is either in the middle or let us fork your offering and work with you on a project or fork our offering." Just get financially interdependent with some people, I think is a good first step.

**Sam:**    	That's a good idea.

**Derek:**    	Leverage the shit out of everything. Leverage the shit out of [OuiShare](http://ouishare.net/en), and Enspiral if it helps, like you can come this year and talk at Open Source Open Society for example and put that shit way up on your website.

Any little thing, that's probably your first step. Ben already does consulting so he has some sense of what the money flow is.

Off the back of POC21 there'll be fucktons of consulting gigs. Ben will not be able to do all of them himself, nor will he want to. If you guys can work together to field that series of responses and make them into something, I think that could be really great. Does that make sense?

**Sam:**    	Yeah.

**Derek:**    	In terms of delivery, that's just, "You'll figure it out as you go," that's kind of the product development. Yeah. Get a clear offering of what you're providing. I think probably the first step to that is getting a clear idea about what Ben is providing and what… 

I can show you want Loomio collaboration training looks like, the product sheet. It's basically just a document with a series of workshops. The workshop is like, "Here's the purpose of this workshop, is to help you find out how fucked your organizational decision making structure is, and to get you started on paying us to do five more workshops to help you make your organization a little bit more collaborative."

**Sam:**    	I think that also helps to manage expectations on the other side as well in terms of, if a company is hiring us and they're like, "Ah, we've heard about this cool open source idea," and like, "Tesla is doing it," and everyone is like, "Free innovation. Money, money, money!" then we turn up and be like, "Actually it's quite a lot of work. You need to build a community. You know what a community is? Okay." 

If people expect us to just deliver a truck of money to their door, then it's not going to work out. If we frame it in terms of, "This is a way of understanding a growing movement, understanding the areas for potential development, developing a pilot project or working with particular people,"

**Derek:**    	What I'm seeing there is ... This is me just projecting as a [green hat](https://en.wikipedia.org/wiki/Six_Thinking_Hats), which is just generativity. One of the things we have in our Loomio consulting trainings is like a, "Choose your own adventure." The first thing we always do is a discovery workshop. We give you an introduction to the topic of collaborative decision making. We learn about your organization, the problems that you're having, the successes that you've had, get intimately familiar with some of your situations. What I'm hearing from you is that's a thing you've already done. 

The second piece is there's actually, I think, a really interesting crossover between what you're doing and what Ben is doing because what Ben is doing is business consulting on strategy which is like ... In some ways it's like a level two. You're like, "Cool. We'll give you the Open Source Circular Economy workshop. It's three grand for four hours. We're going to teach you all about open source." One thing that came to my mind while you were talking about that is ... You say people get, they're not as excited because you're telling them, "It's a whole bunch of work and it's all these things."

**Sam:**    	Often they think that people are just working on open source for free because they're idiots, basically. 

**Derek:**    	Yeah. One angle that I would encourage you to consider is emphasizing the threat of open source. Be like, "Cool. You can ignore this and you can go about doing your circular economy in this closed-source way, but here's what's going to happen when one big player really nails open source and wipes your entire business model out."

Really starting to bridge that gap between the 101 and the strategy and give them a way to see how this could actually be really relevant to the business.

**Sam:**    	So not "Hey, while you're doing this, there's a bunch of scrappy activists in the corner doing something weird," but instead like, "This is something that you need to get involved in.""

**Derek:**    	And bringing ... You've got an arsenal  of examples, right? You've got [Linux](https://en.wikipedia.org/wiki/Linux#Commercial_and_popular_uptake). You're like, “Imagine yourself as the company doing Microsoft Servers. That's what you are right now. No one has developed the Linux of this thing yet. As soon as they do, you're fucked, your whole market share is gone, so let's have a really strategic conversation about what you can do to collaborate with those other guys who are also fucked.”

There's a really interesting psychological thing about this, which is kind of a sales fundraising strategy which [Zaid Hassan](http://www.social-labs.com/the-author/) told me about. He runs these things called social labs. They're basically like an innovation ecosystem development tool that he runs for governments. One of the ones that they did was the International Food Lab. 

What they do is they pick a complex problem, which is like, "OK. We're unsustainably producing food. We're destroying the land. Many people aren't getting enough food as they need to live." They go to companies and governments and they say, "If only 1% of the predicted amount of damage to the world happens as this thing is predicted to do, it will cost you a billion dollars a year. It will likely cause a lot more than 1%." Then he's like, "So give us 1% of 1%, and we'll start working on solutions."

**Sam:**    	Nice.

**Derek:**    	There's this really hard pitch which is about the systematic reality of the situation that somebody else is in. I think you could really leverage that from your particular angle of being an open source, circular economy group. It sounds like Ben does most of his stuff in these two first tiers. The third tier is probably like somebody is interested in developing a community and you're like, "Cool. We're going to be community development consultants because you also don't know how to do that." 

One thing you could explore is actually starting to prototype some of these offerings and show them to people who are close to you who know the business landscape. Be like, "This is a shitty online document, that has our three products on it. Does this look compelling to you? How about these price points?" They're like, "That one is cool. But what the fuck is that? This doesn't make any sense." I'm sure that Ben will have a similar thing.

**Sam:**    	Yeah. We've done community building workshops, with open source hardware, like with makers before, which has been quite good, and that's been beneficial for them. We haven't done it with big businesses yet. I imagine we would need to do a little bit of a rejig in that situation.

**Derek:**    	It sounds like you've got enough to put together a website with an offering category, which is workshops, your consulting strategy, maybe fork some of what Ben is doing, and the community development stuff. You can totally leverage anything that we're doing for community development stuff because we have a bunch of stuff. 

**Sam:**    	One thing I'm wondering about is, "How does this look on the website?"

**Sam:**    	My concern is, yes, a company like Philips or Veolia or Coca-Cola or whoever it might be, who is interested in circular economy, goes to the website, sees this new thing, and goes like, "Oh, cool, they do consulting. Great." 

But what if someone in Bangladesh who is interested in circular economy goes on there, and they might be the kind of person who would run an open source project, who would set up their own local chapter, but they go on there and they see, "Oh, hang on, there's all this consultancy stuff on there. Is this just [Cradle-to-Cradle](http://www.c2ccertified.org/)?" 

How do you think that should be structured in a way that new people to the community don't feel like this is a profit-oriented business that's ...

**Tim:**    	That's the part I'm not sure of. Are we talking here about Open It Agency doing consultancy or OSCEdays doing consultancy?

**Derek:**    	I don't know.

**Sam:**    	Neither do I.

**Tim:**    	If it's Open It, we don't see it on OSCEdays, and that's fine. If it's with the OSCEdays, that's something else.

**Derek:**    	Yeah, the company doing this consulting might just be a partner on the OSCEdays website. It doesn't have to be more than that.

**Sam:**    	And that would be like Open It, if 10% of our revenue goes to OSCEdays.

**Derek:**    	I think this is an incredibly strong foundation on which to build a consultancy, if you have some sales capacity.  To answer your question, we have the same issue in Loomio. 

In general what we do is basically creating the right call to actions in the right places. Different people will read your website differently. A person who's interested in the local chapter will almost certainly skip right over consultancy stuff. As long as you have something which is like, "This is our community building place. Click this button to start a local chapter," you've got some part which is like, "Are you a business? Dah, dah, dah, dah, dah." Right? 

Immediately somebody who is starting a local chapter will scroll right over that. I wouldn't worry too much. I think your sensibility for these things will be the filter. If you make it and you're happy with it, it'll be fine. It could be just a tab which says like, "Home," and it's got the mission, it's got some stuff at the bottom, it's got the consultancy, but one of the tags is services. You click on that and it just takes you to the bottom. Done.

**Sam:**    	I imagine the way that we would start is not form any official services thing for a year or so.

**Derek:**    	Yeah, yeah, totally.

**Sam:**    	Start off with Open It Agency gives 10% to OSCEdays. If Sharon or Erica or Tim or anyone else's business also gets money for something related to Open Source Circular Economy, they do the same thing. Then we can gradually start the community CoBudget process.

I also think it's easier to pivot to having consultancy on the website if the first step is money goes from Open It Agency to OSCEdays. There's money in there that's being used in the community, that's allocated to mission projects. Then people see like, "Oh, okay. This is actually a helpful relationship."

**Derek:**    	That's such an elegant solution, isn't it? I find it really nice because then when you invite someone into your community, now they've got access to financial resources. I find it to be a really good way to get around the problem of, "Is this service company going to lose the mission?" It's like, "Okay, if you guys go off and decide to make heaps of money for three years, it's actually great." We can all keep doing the mission work. What it means is you're going to be busy doing consulting and I'm going to be raising funding buckets for projects

**Derek:**    	I think that starting the gift culture with a big gift is a good way to start.

**Sam:**    	Rather than, "I promise I'll contribute money to OSCEdays later if you just let me stick my consultancy things on the front page and earn money from it." It's much better to do it the other way around. 

**Derek:**    	Yeah. I also think Ben seems really aligned as well as the [Open State](http://www.openstate.cc/) people. They're going to get heaps of new opportunities. They don't actually have the workshop skills and open source knowledge that you guys do, but they have communication skills.

There's a bunch of financial opportunities which are going to come from this thing. How can you create a system in which all of these financial opportunities are not just going to benefit the individuals or the individual organizations that they're being generated by? It would be really easy for this to become, well, a couple of people get a lot more consulting, and that's it. Realistically, that means a bunch of other consulting gigs get dropped. It's not even an efficient situation.

Working out some way of collaborating between all of those people, that's one big question. We have some ideas about that we do. I'm sure that OuiShare has some ways that they do it, but that financial interdependence piece feels really important.

**Sam:**    	Cool. That helps a lot I think. 

**Derek:**    	One thing I can do is I can send you all of the Loomio materials. It's really uncomplicated. It's like a Google doc with a logo and some nice fonts and some tables which just show like, "This is what the workshop is. Here's a half-day workshop. Here's a full-day workshop. Here's the outcomes." It's like three grand, six grand. Great. It actually works. It's pretty nuts. 

You have a great conversation with somebody and send to them "Cool. If you want to do something with your company, here's a product sheet." They can immediately take that and be like, "Yeah. I've got an expenditure budget. As long as it's not over 10K I can pay these guys to come in and give us the 101."

As for all of the other shit, which is like admin and invoicing and legal, you guys will figure it out as you go. For now it's probably premature to put in all of the infrastructure.

The same with the distributed consultancy stuff. It feels like one other big strategic move you could make is ... One thing you can bring to the table with Ben and with Open State and with everyone is like, "Okay, we've got a massive amount of content from OSCEdays. We've got all of this content from POC21. We've got all of this content from OuiShare. What does it look like for us to create a channel in which all of these things are ..."

**Tim:**    	For me that's going to be super hard to do.

**Sam:**    	I think this is where, for example, the Booksprint would be a really good step.

**Derek:**    	Booksprint?

**Tim:**    	For the OSCEdays?

**Sam:**    	For OSCE, yeah. Basically a [Booksprint](http://booksprints.net/) is a facilitative process where between five to twelve people get together for three to five days and write a book. 

**Derek:**    	Cool.

**Sam:**    	It's based largely on accumulated existing knowledge. You can bring along the books and things that you might need to reference through it, but you can't prepare beforehand. You go in there and the facilitator explains how it's all going to work, so that's really important if you've got the CEO and then the intern or someone else. Often the intern will stay quiet and the CEO will be like, "OK, I'm just going to write the table of contents." The facilitator is there to say, "Nope. We're going to listen to everybody. We're going to follow a structure. Everyone will take responsibility for different parts, we'll agree on who's writing style suits most and we'll adapt ourselves to that person's writing style."

They've got illustrators they bring in who are used to the workflow. It's a really slick process. It came out of writing [F/LOSS Manuals](http://en.flossmanuals.net/)...

**Derek:**    	Really?

**Sam:**    	...and has developed into a lot of other stuff for NGOs and organizations. And for the amount of time and energy saved, if we were going to try write a book by ourselves for example, it's very worthwhile.

**Tim:**    	What do you want to put in this book?

**Derek:**    	A book is a great asset.

**Sam:**    	A book is something that people who come from traditional organizations read more than websites and Discourse forums. In that book, I imagine that as an in depth, footnoted version of our mission statement basically, our mission statement turned into the length of a book with each idea fully fleshed-out so you can actually convince people and show them the different ways in which it works and why it works. Our mission statement is still quite shorthand. It still kind of explains things in a very "That's just how it is" kind of way. 

I think a book will at least present the main structure for telling the story of why open source is so important to the circular economy and how we see it developing in the future. Then, because it's all developed digitally and all of the software that is used for writing a Booksprint book is open source, we can basically host that as our Version 1.0 book. As we develop projects and examples and evidence, that can be brought into making the book more useful and powerful, basically.

**Derek:**    	To bring the business development into it, the book, even if it's an extended mission statement and almost especially so, is the ultimate consultant's asset. 

You are the team that wrote the book on the thing. Like, "Oh, you want to do circular economy? I'll just Google it. Huh,  it corrected me to Open Source Circular Economy, that's interesting. Click. First link, your website or the book. Download. Download." In the book it's like, "Made possible by the foundation, the consulting wing, the partners."

**Tim:**    	It could be the first project we develop.

**Derek:**    	I think from a strategic perspective, because it's so early in the circular economy development, that idea, to some degree if you can dominate the content space, you can define what credibility looks like. Like, "We wrote this book and we've got the largest community in the world doing open source community economy work. We've worked with these companies and we've got these partners." 

It's definitely enough of a set of assets that can get you the workshop and then the consultancy gigs. Then once you're a strategic advisor, you actually have the position of being able to be like, "Cool. We're advising these four companies and they actually all have this one business process that they could optimize if they open sourced everything and put it in a framework."

**Sam:**    	To me, getting to grips with Open Source Circular Economy is kind of like trying to understand evolution for the first time, if you've never thought about it. It sounds unbelievably complex, and it works on a time scale... it works on scales that you can't get your head around, but at its core it is remarkably simple. When you understand that core, you can really just look at a lot of different processes and really see how they work. Just having a one-pager website or something isn't really enough to bring people into a new and complex concept, "First, there's open source," then another new concept, "There's circular economy," and then another new concept which is how open source actually applies to the economy. You need to give people time to digest it. You need to give them example after example after example on Every. Single. Different. Point. that you want to make. For that you need a sustained approach,  you know "I'm reading a book. The internet is not distracting me. I'm in my own space and I've got time to focus my brain on it."

**Derek:**    	Or "I'm in a workshop." Or "The book was the hook and I actually read it just before the workshop because we paid for the workshop and they said, 'Read the book before the workshop.'" Yeah. It sounds like a fucking rad project. 

**Tim:**    	Let's do it.

**Derek:**    	Definitely talk to the Open State guys too about how to make the communications aspect of it. 

**Sam:**    	Also because some things in terms of our style aan our website, there's lots of stuff there and our design is not at all unified. 

**Derek:**    	One thing I'm really excited about in the after POC21 is finding a way to align services, like Open State, OuiShare are really good at external communications. Wow, so much better than we are, but they don't have any programmers and we have an army of programmers. So finding ways to align those things so that we can benefit from each other's services, generate that financial interdependence that actually allows everybody to do more. 

**Sam:**    	Cool. I think that's enough to be going on with.

**Derek:**    	Cool. 
